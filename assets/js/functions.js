$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(function () {
    $('[data-toggle="popover"]').popover()
})

$( '[data-toggle="sizePopover"]' ).each(function(index, item){
    $(item).popover({
        content: $(this).children('.data-content').html(),
        html: true
    });
});