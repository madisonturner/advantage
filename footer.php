    <footer class="footer">
      <div class="container">
        <div class="row justify-content-lg-center">
          <div class="col-12 col-md-4 col-lg-auto flex-fill">
            <a href="/"><img src="assets/img/logo-rect-desktop.png" class="logo-footer" alt="advanatge self storage"></a>
            <div class="d-block"><a class="footer-social-link" href="#"><span class="fab fa-facebook-square fa-3x mt-3"></span><span class="sr-only">facebook</span></a><a class="footer-social-link" href="#"><span class="fab fa-twitter-square fa-3x ml-2 mt-3"></span><span class="sr-only">twitter</span></a><a class="footer-social-link" href="#"><span class="fab fa-instagram fa-3x ml-2 mt-3"></span><span class="sr-only">instagram</span></a></div>
          </div>
          <div class="col-12 col-md-4 col-lg-auto flex-fill">
            <ul class="list-unstyled footer-link-group">
              <li class="footer-link-header">Locations</li>
              <li><a href="#">Texas</a></li>
              <li><a href="#">Colorado</a></li>
              <li><a href="#">Arizona</a></li>
            </ul>
          </div>
          <div class="col-12 col-md-4 col-lg-auto flex-fill">
            <ul class="list-unstyled footer-link-group">
              <li class="footer-link-header">What We Offer</li>
              <li><a href="#">Storage Unit Features</a></li>
              <li><a href="#">Packing Supplies</a></li>
              <li><a href="#">Tenant Insurance</a></li>
              <li><a href="#">Moving Services</a></li>
            </ul>
          </div>
          <div class="col-12 col-md-4 col-lg-auto flex-fill">
            <ul class="list-unstyled footer-link-group">
              <li class="footer-link-header">Resources</li>
              <li><a href="#">Storage Tips</a></li>
              <li><a href="#">Packing Tips</a></li>
              <li><a href="#">FAQ</a></li>
            </ul>
          </div>
          <div class="col-12 col-md-4 col-lg-auto flex-fill">
            <ul class="list-unstyled footer-link-group">
              <li class="footer-link-header">About Us</li>
              <li><a href="#">Who We Are</a></li>
              <li><a href="#">Giving Back</a></li>
              <li><a href="#">Careers</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="assets/js/lightgallery.min.js"></script>
    <script src="assets/js/functions.min.js"></script>
  </body>
</html>