  <?php include 'header.php';?>
      <!-- Pre Footer Search -->
      <div class="global-search">
        <div class="d-lg-flex justify-content-center align-items-center">
          <h1 class="market-header d-lg-inline mr-lg-5 mb-3 mb-lg-0">6 storage facilities near Arizona</h1>
          <div class="d-flex justify-content-center align-items-center">
            <div class="location-search">
              <div class="form-group mb-1">
                <label for="locationSearch1">Zip or City, State</label>
                <div class="d-flex justify-content-center align-items-center">
                  <span class="fas fa-search mr-2"></span>
                  <input type="text" class="form-control" id="locationSearch1" placeholder="Arizona">
                </div>
              </div>
            </div>
            <button class="btn btn-location"><span class="fas fa-location-arrow"></span><span class="sr-only">Use Your Location</span></button>
            <button class="btn btn-search btn-market-search ml-1"><span class="search-icon"><span class="fas fa-search"></span></span><span class="search-txt">Search Again</span></button>
          </div>
        </div>
      </div>
      <div class="container-fluid bg-white">
        <div class="row p-lg-5">
          <div class="col-lg-8 col-xl-9 order-last order-lg-first">
            <div class="market-table">
              <div class="row card-group">
                <div class="col-12 col-sm-6 col-xl-4">
                  <div class="card market-card">
                    <img src="assets/img/fac-image.jpg" class="card-img-top" alt="facility image">
                    <div class="card-body">
                      <h5 class="card-title">Advantage Storage in Avondale</h5>
                      <address class="card-text">1101 S Avondale Blvd,<br />Avondale, AZ 85323<br />623-925-0177</address>
                      <a class="btn btn-secondary stretched-link" href="#zero">Available Units</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-4">
                  <div class="card market-card">
                    <img src="assets/img/fac-image.jpg" class="card-img-top" alt="facility image">
                    <div class="card-body">
                      <h5 class="card-title">Advantage Storage in Avondale</h5>
                      <address class="card-text">1101 S Avondale Blvd,<br />Avondale, AZ 85323<br />623-925-0177</address>
                      <a class="btn btn-secondary stretched-link" href="#one">Available Units</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-4">
                  <div class="card market-card">
                    <img src="assets/img/fac-image.jpg" class="card-img-top" alt="facility image">
                    <div class="card-body">
                      <h5 class="card-title">Advantage Storage in Avondale</h5>
                      <address class="card-text">1101 S Avondale Blvd,<br />Avondale, AZ 85323<br />623-925-0177</address>
                      <a class="btn btn-secondary stretched-link" href="#two">Available Units</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-4">
                  <div class="card market-card">
                    <img src="assets/img/fac-image.jpg" class="card-img-top" alt="facility image">
                    <div class="card-body">
                      <h5 class="card-title">Advantage Storage in Avondale</h5>
                      <address class="card-text">1101 S Avondale Blvd,<br />Avondale, AZ 85323<br />623-925-0177</address>
                      <a class="btn btn-secondary stretched-link" href="#three">Available Units</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-4">
                  <div class="card market-card">
                    <img src="assets/img/fac-image.jpg" class="card-img-top" alt="facility image">
                    <div class="card-body">
                      <h5 class="card-title">Advantage Storage in Avondale</h5>
                      <address class="card-text">1101 S Avondale Blvd,<br />Avondale, AZ 85323<br />623-925-0177</address>
                      <a class="btn btn-secondary stretched-link" href="#four">Available Units</a>
                    </div>
                  </div>
                </div>
              </div><!-- /row -->
            </div><!-- /market table -->
          </div>
          <div class="col-lg-4 col-xl-3 order-first order-lg-last">
            <div class="d-flex d-lg-block justify-content-between align-items-center">
              <div class="btn btn-filters pt-3 pb-3 pl-0 pr-0 d-lg-none"><span class="fal fa-sliders-h fa-lg brand-primary mr-3"></span> Filters</div>
              <div class="map-btn-wrapper"><a href="#" class="btn btn-primary map-btn p-0 p-lg-3"><span class="far fa-map fa-lg  mr-3 d-inline-block d-lg-none brand-primary"></span>View On Map</a></div>
            </div>
            <div class="market-filters mb-4">
              <h2 class="filters-title mt-lg-4">Filters</h2>
              <h3 class="features-title mt-3">Features</h3>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="FreeLock">
                <label class="form-check-label" for="FreeLock">Free Lock</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="24HourGateAccess">
                <label class="form-check-label" for="24HourGateAccess">24 Hour Gate Access</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="24HourSecurityMonitoring">
                <label class="form-check-label" for="24HourSecurityMonitoring">24 Hour Security Monitoring</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="ClimateControl">
                <label class="form-check-label" for="ClimateControl">Climate Control</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="Access7DaysaWeek">
                <label class="form-check-label" for="Access7DaysaWeek">Access 7 Days a Week</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="FriendlyProfessionalStaff">
                <label class="form-check-label" for="FriendlyProfessionalStaff">Friendly Professional Staff</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="CoveredDriveThruAccess">
                <label class="form-check-label" for="CoveredDriveThruAccess">Covered Drive-Thru Access</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="RVStorage">
                <label class="form-check-label" for="RVStorage">RV Storage</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="FencedLighted">
                <label class="form-check-label" for="FencedLighted">Fenced &amp; Lighted</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="GroundLevelUnits">
                <label class="form-check-label" for="GroundLevelUnits">Ground Level Units</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="HighCeilings">
                <label class="form-check-label" for="HighCeilings">High Ceilings</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="MajorCreditCardsAccepted">
                <label class="form-check-label" for="MajorCreditCardsAccepted">Major Credit Cards Accepted</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="MonthtoMonthRentals">
                <label class="form-check-label" for="MonthtoMonthRentals">Month to Month Rentals</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="NoDeposits">
                <label class="form-check-label" for="NoDeposits">No Deposits</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="OnlineBillPayment">
                <label class="form-check-label" for="OnlineBillPayment">Online Bill Payment</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="WideDriveways">
                <label class="form-check-label" for="WideDriveways">Wide Driveways</label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="marketMap"></div>
    </main>
  <?php include 'footer.php';?>

<script>

  var map, infowindow, gmarkers = [];
  function initMap() {

    var locations = [['<div class="map-info-window"><h4 class="infowindow-title">Advantage Storage in Allen, Texas</h4><p class="infowindow-p">1210 W McDermott Dr<br />Allen, TX 75013<br />(972)-905-6878</p><a role="button" class="btn btn-primary btn-sm btn-infowin" href="https://www.google.com/maps/dir//Advantage+Storage+-+Allen,+1210+W+McDermott+Dr,+Allen,+TX+75013/@33.102257,-96.6953054,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x864c176d52d98c87:0x1d470721bd392133!2m2!1d-96.6953089!2d33.1020718">Get Directions</a> </div>', 33.102257,-96.6953054, 1]];
    var centerLatLng = {lat: 33.102257, lng: -96.6953054};
    var mapCanvas = document.getElementById('marketMap');
    var mapOptions = {
      center: centerLatLng,
      zoom: 15,
      mapTypeControl: false,
      //disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(mapCanvas, mapOptions);
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 275
      });
    createMarkers(locations,infowindow);

  }

  function createMarkers(loc,infowin) {

    var marker, i;
    var numberMarkerImg = {
      url: 'assets/img/pin.png'
    };

    for (i = 0; i < loc.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(loc[i][1], loc[i][2]),
        title: 'Click for more info',
        icon: numberMarkerImg, // adds a custom marker image
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          infowin.setContent(loc[i][0]);
          infowin.open(map, marker);
        }
      })(marker, i));

      gmarkers.push(marker);
    }

    // Creates the CLOSE MAP button
    function CenterControl(controlDiv, map) {
      // Set CSS for the button border.
      var controlUI = document.createElement('div');
      controlUI.style.backgroundColor = '#fff';
      controlUI.style.border = '2px solid #fff';
      controlUI.style.borderRadius = '3px';
      controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
      controlUI.style.cursor = 'pointer';
      controlUI.style.marginTop = '10px';
      controlUI.style.marginLeft = '10px';
      controlUI.style.textAlign = 'center';
      controlUI.title = 'Click to close the map';
      controlDiv.appendChild(controlUI);
      // Set CSS for the button interior.
      var controlText = document.createElement('div');
      controlText.style.color = 'rgb(25,25,25)';
      controlText.style.fontFamily = 'Arial,sans-serif';
      controlText.style.fontSize = '15px';
      controlText.style.fontWeight = '500';
      controlText.style.lineHeight = '38px';
      controlText.style.paddingLeft = '15px';
      controlText.style.paddingRight = '15px';
      controlText.innerHTML = '<span class="fas fa-angle-left mr-2"></span> Back to Results';
      controlUI.appendChild(controlText);
      // Setup the click event listeners: hides the map off viewport
      controlUI.addEventListener('click', function(e) {
        e.preventDefault();
        $('body').removeClass('noscroll');
        $("#marketMap").removeClass('tall').css('top', '100%');
      });
    }

    // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);
        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(centerControlDiv);



  }

  $(document).ready(function(){

    // draws the map
    function drawMap() {
        var currCenter = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setCenter(currCenter);
    }

    // locks the body, opens the map
    $(".map-btn").click(function(e){
      e.preventDefault();
      $('body').addClass('noscroll');
      $("#marketMap").addClass('tall').animate({top:'0px'},300, drawMap);
    });

    // toggle the filters on mobile
    $('.btn-filters').on('click', function() {
      $('.market-filters').toggle();
    })
  });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJz0Oxwx75_kS1j3J67KV_FsWsT8kxpFA&callback=initMap" async defer></script>




