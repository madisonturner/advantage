  <?php include 'header.php';?>

      <div class="hero homepage-hero d-flex flex-column justify-content-center align-items-center">
        <div class="homepage-hero-content">
          <h1 class="mb-4">Your One-stop Solution to State-of-the-art Storage</h1>
          <div class="d-flex justify-content-center align-items-center">
            <div class="location-search">
              <div class="form-group mb-1">
                <label for="locationSearch1">Zip or City, State</label>
                <div class="d-flex justify-content-center align-items-center">
                  <span class="fas fa-search mr-2"></span>
                  <input type="text" class="form-control" id="locationSearch1">
                </div>
              </div>
            </div>
            <button class="btn btn-location"><span class="fas fa-location-arrow"></span><span class="sr-only">Use Your Location</span></button>
            <button class="btn btn-search ml-1"><span class="search-icon"><span class="fas fa-search"></span></span><span class="search-txt">Find Storage</span></button>
          </div>
        </div>
        <div class="hero-quick-links">Quick links: <a href="#">Texas</a>, <a href="#">Arizona</a>, <a href="#">Colorado</a></div>
      </div>

      <!-- Home Features -->
      <div class="home-features pb-0 pb-lg-5">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center"><span class="home-subheader text-center">Advantage Locations Feature</span></div>
            <div class="col-12 col-md-6 col-lg-3 d-flex align-items-stretch">
              <div class="card home-feature mb-4 mb-lg-0">
                <span class="fas fa-shield-alt fa-5x brand-primary"></span>
                <div class="home-feature-txt mb-3">
                  <span class="home-feature-title">Exceptionally Clean Units</span>
                  <p class="home-feature-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 d-flex align-items-stretch">
              <div class="card home-feature mb-4 mb-lg-0">
                <div class="home-feature-txt mb-3">
                  <span class="fas fa-bell fa-5x brand-secondary"></span>
                  <span class="home-feature-title">Top-Notch Security</span>
                  <p class="home-feature-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 d-flex align-items-stretch">
              <div class="card home-feature mb-4 mb-lg-0">
                <div class="home-feature-txt mb-3">
                  <span class="fas fa-fire-extinguisher fa-5x brand-primary"></span>
                  <span class="home-feature-title">Exceptional Customer Service</span>
                  <p class="home-feature-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
              </div> 
            </div>
            <div class="col-12 col-md-6 col-lg-3 d-flex align-items-stretch">
              <div class="card home-feature mb-4 mb-lg-0">
                <div class="home-feature-txt mb-3">
                  <span class="fas fa-map-marker-alt fa-5x brand-secondary"></span>
                  <span class="home-feature-title">Convenient Locations</span>
                  <p class="home-feature-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Storage Tools -->
      <div class="home-storage-tools pb-0 pb-lg-5">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-4 d-flex align-items-stretch">
              <div class="d-block home-storage-tool mb-4 mb-lg-0">
                <img src="assets/img/home-feature-bg-01.jpg" class="card-img-top img-fluid" alt="Storage Tips">
                <div class="home-storage-tools-txt">
                  <span class="home-storage-tools-title">Storage Tips</span>
                  <p class="home-storage-tools-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                  <a href="#">Storage Tips</a>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 d-flex align-items-stretch">
              <div class="d-block home-storage-tool mb-4 mb-lg-0">
                <img src="assets/img/home-feature-bg-02.jpg" class="card-img-top img-fluid" alt="Packing Tips">
                <div class="home-storage-tools-txt">
                  <span class="home-storage-tools-title">Packing Tips</span>
                  <p class="home-storage-tools-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                  <a href="#">Packing Tips</a>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 d-flex align-items-stretch">
              <div class="d-block home-storage-tool mb-4 mb-lg-0">
                <img src="assets/img/home-feature-bg-03.jpg" class="card-img-top img-fluid" alt="FAQ">
                <div class="home-storage-tools-txt">
                  <span class="home-storage-tools-title">FAQ</span>
                  <p class="home-storage-tools-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                  <a href="#">FAQ</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Pre Footer Search -->
      <div class="pre-footer-search">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center mb-3">
              <h2>Make Your Move with Advantage</h2>
            </div>
            <div class="col-12">
            <div class="d-flex justify-content-center align-items-center">
              <div class="location-search">
                <div class="form-group mb-1">
                  <label for="locationSearch2">Zip or City, State</label>
                  <div class="d-flex justify-content-center align-items-center">
                    <span class="fas fa-search mr-2"></span>
                    <input type="text" class="form-control" id="locationSearch2">
                  </div>
                </div>
              </div>
              <button class="btn btn-location"><span class="fas fa-location-arrow"></span><span class="sr-only">Use Your Location</span></button>
              <button class="btn btn-search ml-1"><span class="search-icon"><span class="fas fa-search"></span></span><span class="search-txt">Find Storage</span></button>
            </div>
          </div>
        </div>
      </div>
    </main>
  <?php include 'footer.php';?>
