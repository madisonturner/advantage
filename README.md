# Advantage Self Storage HTML Mockups

## Getting Started

This is the boilerplate for setting up a new design project to use SASS

## Required Software

- **NodeJS/NPM**: https://nodejs.org/en/

## Required NPM Packages

- `npm install node-sass`
- `npm install -g gulp`

## Setting Up A New Project

- `npm init`
This first command will run through the process of setting up a new package.json file. Follow the steps and answer what is applicable to the project.

- `npm install --save-dev gulp`
Adds Gulp as a dev dependencie for the project to run Gulp tasks.
https://www.npmjs.com/package/gulp

- `npm install --save-dev gulp-sass`
SASS plugin for Gulp for processing SASS files. Uses node-sass to compile SASS to CSS. Also adds plugin as a dev dependecy.
https://www.npmjs.com/package/gulp-sass

- `npm install --save-dev gulp-minify`
Minify plugin for Gulp that minifies JavaScript files. Also adds plugin as a dev dependecy for.
https://www.npmjs.com/package/gulp-minify

- `npm install --save-dev gulp-group-css-media-queries`
Gulp plugin of Group CSS Media Queries. This combines all media queries after SASS has been compiled. Also adds the plugin as a dev dependency.

- `npm install --save-dev gulp-clean-css`
Gulp plugin to minify CSS. Also adds plugin as a dev dependency.

- `npm install --save-dev breakpoint-sass`
Plugin for handling breakpoints in SASS. Also adds plugin as a dev dependency.
https://github.com/at-import/breakpoint

- `npm install --save-dev breakpoint-slicer`
Plugin for setting up easy breakpoint slices. Also adds plugin as a dev dependency.
https://github.com/lolmaus/breakpoint-slicer

- `npm install --save-dev gulp-line-ending-corrector`
Gulp plugin for fixing line endings on Windows machines. Also adds plugin as dev dependency.
https://www.npmjs.com/package/gulp-line-ending-corrector

- `npm install --save-dev gulp-rename`
Gulp plugin for renaming minified files. Also adds plugin as dev dependency.
https://www.npmjs.com/package/gulp-rename

- `npm install --save-dev gulp-autoprefixer`
Gulp plugin for adding browser vendor prefixes
https://www.npmjs.com/package/gulp-autoprefixer

## Gulp File Instructions

A preset Gulp File that sets up the workflow for using the above plugins to handle SASS files.

## Running Gulp Tasks

- `gulp` the default task that runs sass, clean, and minify tasks
- `gulp watch` a task watches for changes in all SASS files, then runs the sass and clean tasks when it detects a change
- `gulp sass` a task that compiles sass to css, combines breakpoints, and fixes line endings on windows
- `gulp minify` a task that minifies the JS file