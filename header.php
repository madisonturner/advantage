<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="web-fonts-with-css/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/css/lightgallery.min.css">
    <link rel="stylesheet" href="style.min.css">
    <title>Advantage Self Storage | Welcome</title>
  </head>
  <body>
    <header role="banner">
      <a href="#maincontent" id="skiptocontent">skip to main content</a>
      <div class="bg-white header-wrapper">
        <div class="container p-0">
          <nav class="navbar navbar-white navbar-expand-xl justify-content-between">
            <a href="/" class="navbar-brand mr-0"><img src="assets/img/logo-desktop.png" alt="Advantage Storage"></a> 
            <button class="btn btn-primary pay-online-sm ml-auto mr-2" href="#"><span class="fas fa-lock-alt mr-2"></span> Pay Online</button>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar2"><span class="hamburger-line"></span><span class="hamburger-line"></span><span class="hamburger-line"></span><span class="sr-only">Menu</span></button>
            <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
              <ul class="navbar-nav mx-xl-auto text-center">
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="locationdropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Locations <span class="fas fa-angle-down d-inline-block d-xl-none"></span></a>
                  <div class="dropdown-menu" aria-labelledby="locationdropdown">
                    <a class="dropdown-item" href="#">Texas</a>
                    <a class="dropdown-item" href="#">Colorado</a>
                    <a class="dropdown-item" href="#">Arizona</a>
                  </div>
                </li>
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="whatweofferdropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">What We Offer <span class="fas fa-angle-down d-inline-block d-xl-none"></span></a>
                  <div class="dropdown-menu" aria-labelledby="whatweofferdropdown">
                    <a class="dropdown-item" href="#">Storage Unit Features</a>
                    <a class="dropdown-item" href="#">Packing Supplies</a>
                    <a class="dropdown-item" href="#">Tenant Insurance</a>
                    <a class="dropdown-item" href="#">Moving Services</a>
                  </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="#">Resources</a></li>
                <li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
              </ul>
              <ul class="nav navbar-nav flex-row justify-content-center flex-nowrap">
                <li class="nav-item"><button class="btn btn-primary pay-online" href="#"><span class="fas fa-lock-alt mr-2"></span> Pay Online</button></li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>
    <main id="maincontent">
