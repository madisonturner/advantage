<?php include 'header.php';?>
<div class="hero facility-hero" style="background-image: url(assets/img/facility-bg-desktop.jpg);"></div>
<div class="container">
	<div class="row">
		<div class="col-12 col-lg-4">
			<div class="facility-info bg-white p-4">
				<h1>Advantage Storage in Allen</h1>
				<address>1210 W McDermott Dr<br />Allen, TX 75013<br />(972) - 905 - 6878</address>
				<button class="btn btn-secondary view-units-trigger">Rent or Reserve Now</button>
			</div>
			<div class="fac-gallery-bg" style="background-image:url(assets/img/facility-slideshow-thb.jpg);">
				<div id="facility-gallery">
					<a href="assets/img/facility-slideshow-thb.jpg">
						<button class="btn btn-gallery">More Photos <span class="far fa-expand-arrows-alt"></span></button>
					</a>
					<a href="assets/img/home-hero-desktop.jpg"><span class="sr-only">Facility photo 1</span></a>
				</div>
			</div>
			<div class="fac-hours bg-white p-4">
				<ul class="list-unstyled">
					<li class="title">Office Hours</li>
					<li>Mon - Fri : 9:00am - 6:00pm</li>
					<li>Sat: 9:00am - 5:00pm</li>
					<li>Sun: Closed</li>
				</ul>
				<ul class="list-unstyled">
					<li class="title">Access Hours</li>
					<li>Mon - Fri : 9:00am - 6:00pm</li>
				</ul>
				Need Assistance? <a href="#" data-toggle="modal" data-target="#exampleModalCenter">Contact Us</a>
			</div>
		</div>
		<div class="col-12 col-lg-8">
			<div class="fac-tabs">
				<ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
					<li class="nav-item"><a class="nav-link units-tab active" id="units-tab" data-toggle="tab" href="#units" role="tab" aria-controls="units" aria-selected="true">Units</a></li>
					<li class="nav-item"><a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false">Features</a></li>
					<li class="nav-item"><a class="nav-link" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="false">About</a></li>
					<li class="nav-item"><a class="nav-link" id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="false">Map</a></li>
				</ul>
			</div>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane units-content fade show active" id="units" role="tabpanel" aria-labelledby="units-tab">
			  	<button class="btn view-unit-filters-trigger btn-sm d-flex align-items-center w-100"><span class="far fa-sliders-h mr-2"></span> Filters <span class="far fa-angle-down fa-2x ml-auto"></span></button>
			  	<div class="unit-size-filters flex-column flex-md-row">
				  	<button class="btn btn-fac-size active">All Sizes</button>
				  	<button class="btn btn-fac-size">Small</button>
				  	<button class="btn btn-fac-size">Medium</button>
				  	<button class="btn btn-fac-size">Large</button>
				  	<button class="btn btn-fac-size">Parking</button>
				</div>
			  	<div class="units-table">
			  		<!-- unit row -->
			  		<div class="row justify-content-center align-items-start unit-row ml-0 mr-0">
			  			<div class="col-12 col-md-5 d-flex align-self-center unit-hr">
				  			<div class="unit-info">
				  				<span class="title d-block">5' x 5'</span>
				  				<a tabindex="0" class="d-inline-block" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Small Unit" data-content="Small units are units smaller than 50 square feet."><span class="fas fa-info-circle fa-sm"></span> Small Unit</a>
				  			</div>
				  			<div class="unit-features ml-3 ml-md-auto">
				  				<ul class="list-unstyled mb-0">
				  					<li>Drive Up</li>
				  					<li>Climate Controlled</li>
				  					<li>Ground Floor</li>
				  				</ul>
				  			</div>
			  			</div>
				  		<div class="col-12 col-md-6 offset-md-1 d-flex align-items-center pt-2 pr-2 pr-md-0 pt-md-0">
				  			<div class="unit-promo"><span>Hurry, only 3 left!</span></div>
				  			<div class="unit-price ml-auto">
				  				<div class="text-center mb-2 mb-md-0">Starting at <div class="price"><sup>$</sup><span>45</span></div></div>
				  				<button class="btn btn-primary">Select</button>
				  			</div>
				  		</div>
			  		</div>
			  		<!-- end unit row -->
			  		<!-- unit row -->
			  		<div class="row justify-content-center align-items-start unit-row ml-0 mr-0">
			  			<div class="col-12 col-md-5 d-flex align-self-center unit-hr">
				  			<div class="unit-info">
				  				<span class="title d-block">15' x 15'</span>
				  				<a tabindex="0" class="d-inline-block" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Medium Unit" data-content="Medium units are larger than 100 square feet but smaller than 200 square feet."><span class="fas fa-info-circle fa-sm"></span> Medium Unit</a>
				  			</div>
				  			<div class="unit-features ml-3 ml-md-auto">
				  				<ul class="list-unstyled mb-0">
				  					<li>Drive Up</li>
				  					<li>Climate Controlled</li>
				  					<li>Ground Floor</li>
				  				</ul>
				  			</div>
			  			</div>
				  		<div class="col-12 col-md-6 offset-md-1 d-flex align-items-center pt-2 pr-2 pr-md-0 pt-md-0">
				  			<div class="unit-promo"><span>Hurry, only 2 left!</span></div>
				  			<div class="unit-price ml-auto">
				  				<div class="text-center mb-2 mb-md-0">Starting at <div class="price"><sup>$</sup><span>70</span></div></div>
				  				<button class="btn btn-primary">Select</button>
				  			</div>
				  		</div>
			  		</div>
			  		<!-- end unit row -->
			  		<!-- unit row -->
			  		<div class="row justify-content-center align-items-start unit-row ml-0 mr-0">
			  			<div class="col-12 col-md-5 d-flex align-self-center unit-hr">
				  			<div class="unit-info">
				  				<span class="title d-block">15' x 15'</span>
				  				<a tabindex="0" class="d-inline-block" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Medium Unit" data-content="Medium units are larger than 100 square feet but smaller than 200 square feet."><span class="fas fa-info-circle fa-sm"></span> Medium Unit</a>
				  			</div>
				  			<div class="unit-features ml-3 ml-md-auto">
				  				<ul class="list-unstyled mb-0">
				  					<li>Drive Up</li>
				  					<li>Climate Controlled</li>
				  					<li>Ground Floor</li>
				  				</ul>
				  			</div>
			  			</div>
				  		<div class="col-12 col-md-6 offset-md-1 d-flex align-items-center pt-2 pr-2 pr-md-0 pt-md-0">
				  			<div class="unit-price ml-auto">
				  				<div class="text-center mb-2 mb-md-0">Starting at <div class="price"><sup>$</sup><span>70</span></div></div>
				  				<button class="btn btn-primary">Select</button>
				  			</div>
				  		</div>
			  		</div>
			  		<!-- end unit row -->
			  		<!-- unit row -->
			  		<div class="row justify-content-center align-items-start unit-row ml-0 mr-0">
			  			<div class="col-12 col-md-5 d-flex align-self-center unit-hr">
				  			<div class="unit-info">
				  				<span class="title d-block">25' x 30'</span>
				  				<a tabindex="0" class="d-inline-block" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="Large Unit" data-content="Large units are larger than 200 square feet."><span class="fas fa-info-circle fa-sm"></span> Large Unit</a>
				  			</div>
				  			<div class="unit-features ml-3 ml-md-auto">
				  				<ul class="list-unstyled mb-0">
				  					<li>Drive Up</li>
				  					<li>Climate Controlled</li>
				  					<li>Ground Floor</li>
				  				</ul>
				  			</div>
			  			</div>
				  		<div class="col-12 col-md-6 offset-md-1 d-flex align-items-center pt-2 pr-2 pr-md-0 pt-md-0">
				  			<div class="unit-promo"><span>Hurry, only 1 left!</span></div>
				  			<div class="unit-price ml-auto">
				  				<div class="text-center mb-2 mb-md-0">Starting at <div class="price"><sup>$</sup><span>70</span></div></div>
				  				<button class="btn btn-primary">Select</button>
				  			</div>
				  		</div>
			  		</div>
			  		<!-- end unit row -->
			  	</div>
			  </div>
			  <div class="tab-pane fac-features fade" id="features" role="tabpanel" aria-labelledby="features-tab">
			  	<div class="row align-items-center justify-content-start">
			  		<div class="col-12">
			  			<h2 class="pt-4 mb-4">Advantage Storage Units in Allen Texas Feature</h2>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-shield-alt fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Exceptionally Clean Units</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-bell fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Top-Notch Security</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-fire-extinguisher fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Exceptional Customer Service</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-map-marker-alt fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Convenient Locations</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-map-marker-alt fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Great Value</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-boxes fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Moving Supplies</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-dolly fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Dollies Available</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-truck-couch fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Free Truck</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-bell fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Top-Notch Security</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-shield-alt fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Exceptionally Clean Units</div>
			  		</div>
			  		<div class="col-4 col-md-3col-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-fire-extinguisher fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Exceptional Customer Service</div>
			  		</div>
			  		<div class="col-4 col-md-3 d-flex flex-column align-items-center justify-content-center">
			  			<span class="fas fa-map-marker-alt fa-3x brand-primary"></span>
			  			<div class="text-center fac-feature-txt mb-3">Convenient Locations</div>
			  		</div>
			  	</div>
			  </div>
			  <div class="tab-pane fac-about fade" id="about" role="tabpanel" aria-labelledby="about-tab">
			  	<div class="row align-items-center justify-content-start">
			  		<div class="col-12">
			  			<h2 class="pt-4 pb-2">Welcome to Advantage Storage in Allen Texas</h2>
			  			<p>Advantage Storage—Allen is conveniently located at the intersection of McDermott Dr. and Alma Rd. in thriving Allen, TX. Our location is minutes from access to Highway 75, Business Highway 5 & the Sam Rayburn Tollway (121). We are happy to serve the high demographic communities of Allen, Fairview, & Plano.</p>
						<p>Our facility is minutes from the rapidly expanding <a href="#">McKinney Regional Airport</a>, as well as, prestigious shopping/dining available at Watters Creek & The Villages of Fairview. A number of corporate offices are also in the process of moving their North American headquarters to just minutes from our facility. With such a prestigious business climate as a landscape, Advantage Storage—Allen offers you an exceptional option for short or long-term storage. We are pleased to serve both your personal and business storage needs. We also offer Office Suites.</p>
			  		</div>
			  	</div>
			  </div>
			  <div class="tab-pane fac-map fade" id="map" role="tabpanel" aria-labelledby="map-tab">
			  </div>
			</div>
		</div>
	</div>
</div>
<?php include 'modals.php';?>
<?php include 'footer.php';?>

<script>

	var map, infowindow, gmarkers = [];

	function initMap() {

	  var locations = [['<div class="map-info-window"><h4 class="infowindow-title">Advantage Storage in Allen, Texas</h4><p class="infowindow-p">1210 W McDermott Dr<br />Allen, TX 75013<br />(972)-905-6878</p><a role="button" class="btn btn-primary btn-sm btn-infowin" href="https://www.google.com/maps/dir//Advantage+Storage+-+Allen,+1210+W+McDermott+Dr,+Allen,+TX+75013/@33.102257,-96.6953054,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x864c176d52d98c87:0x1d470721bd392133!2m2!1d-96.6953089!2d33.1020718">Get Directions</a> </div>', 33.102257,-96.6953054, 1]];
	  var centerLatLng = {lat: 33.102257, lng: -96.6953054};
	  var mapCanvas = document.getElementById('map');
	  var mapOptions = {
	    center: centerLatLng,
	    zoom: 15,
	    mapTypeControl: false,
	    //disableDefaultUI: true,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  }

	  map = new google.maps.Map(mapCanvas, mapOptions);
	  var infowindow = new google.maps.InfoWindow({
    		maxWidth: 275
  		});
	  createMarkers(locations,infowindow);

	}

	function createMarkers(loc,infowin) {

	  var marker, i;
	  var numberMarkerImg = {
	    url: 'assets/img/pin.png'
	  };

	  for (i = 0; i < loc.length; i++) {
	    marker = new google.maps.Marker({
	      position: new google.maps.LatLng(loc[i][1], loc[i][2]),
	      title: 'Click for more info',
	      icon: numberMarkerImg, // adds a custom marker image
	      map: map
	    });

	    google.maps.event.addListener(marker, 'click', (function (marker, i) {
	      return function () {
	        infowin.setContent(loc[i][0]);
	        infowin.open(map, marker);
	      }
	    })(marker, i));

	    gmarkers.push(marker);
	  }
	}

  $(document).ready(function(){
    // called after the animation finishes (kinda)
    function drawMap() {
        var currCenter = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setCenter(currCenter);
    }

	// Animate the body to someplace when a certain link or button is clicked
	function scrollToElement(position) {
	    $('body, html').animate({scrollTop: position});
	}

	// scroll to the units
    $('.view-units-trigger').on('click', function(e) {
		e.preventDefault();
		var pos = $('.units-tab').offset().top;
		scrollToElement(pos);
		$('.units-tab').tab('show');
    });

	// lightgallery init
	lightGallery(document.getElementById('facility-gallery'));

    $('.view-unit-filters-trigger').on('click', function(e) {
		e.preventDefault();
		$('.unit-size-filters').toggleClass('d-flex');
	});

  });

</script>

    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJz0Oxwx75_kS1j3J67KV_FsWsT8kxpFA&callback=initMap"></script>






