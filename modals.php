<!-- Facility Contact Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title contact-title">Have a Question?</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" novalidate>
      <div class="modal-body">
        <p>We're here to answer your questions. Simply send your name, contact info and question. We'll get back to you as soon as possible. *All Fields are required.</p> 
        <div class="form-row">
          <div class="col-12 col-lg-6">
            <div class="form-group">
              <label for="firstName">First Name*</label>
              <input type="text" class="form-control" id="firstName" required>
            </div>
          </div>
          <div class="col-12 col-lg-6">
            <div class="form-group">
              <label for="lastName">Last Name*</label>
              <input type="text" class="form-control" id="lastName" required>
            </div>
          </div>
          <div class="col-12 col-lg-12">
            <div class="form-group">
              <label for="customerEmail">Your Email Address*</label>
              <input type="email" class="form-control" id="customerEmail" required>
            </div>
          </div>
          <div class="col-12 col-lg-12">
            <div class="form-group">
              <label for="customerMessage">Your Message*</label>
              <textarea class="form-control" id="customerMessage" rows="3" required></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Send Message</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>



